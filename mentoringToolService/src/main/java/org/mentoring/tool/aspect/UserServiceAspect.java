package org.mentoring.tool.aspect;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.mentoring.model.User;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UserServiceAspect {
	
	@Pointcut("execution(public * createUser(..))")
	public void pointcutMethodCreateUser(){}
	
	@Pointcut("execution(public * updateUser(..))")
	public void pointcutMethodUpdateUser(){}
	
	@Pointcut("within(org.mentoring.tool.service.impl.UserServiceImpl)")
	public void pointcutUserService(){}
	
	@Pointcut("pointcutMethodCreateUser() && pointcutUserService()")
	public void pointcutCreateUser(){}
	
	@Pointcut("pointcutMethodUpdateUser() && pointcutUserService()")
	public void pointcutUpdateUser(){}
	
	@Before("pointcutCreateUser() && args(object)")
	public void setAditionalInformationAboutCreateUser(Object object){
		User user = (User)object;
		user.setDateCreate(new Date());
		user.setCreateByUser("Maks");
	}
	
	@Around("pointcutUpdateUser() && args(object)")
	public Object setAditionalInformationAboutUpdateUser(ProceedingJoinPoint proceedingJoinPoint, Object object) throws Throwable{
		User user = (User)object;
		user.setDateLastModified(new Date());
		user.setLastModifiedByUser("Maks");
		return proceedingJoinPoint.proceed();
	}
	
	@Before("execution(public * save(..))")
	public void loginSaveUser(JoinPoint point){
	System.out.println(point.getSignature().getName());
	}
	
	
}
