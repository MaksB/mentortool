package org.mentoring.tool.service;

import java.util.List;

import org.mentoring.model.Level;
import org.mentoring.model.User;

public interface UserService {

	User createUser(User user);
	User getUserById(Long id);
	User updateUser(User user);
	List<User> getAllUsers();
	void deleteUser(Long id);
	List<User> search(String param);
}
