package org.mentoring.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.mentoring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "Select * From User Where name like :param or email like :param or level like :param", nativeQuery = true)
	public List<User> search(@Param("param") String param);
}
