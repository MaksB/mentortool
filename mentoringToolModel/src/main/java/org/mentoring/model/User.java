package org.mentoring.model;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Data
public class User extends BaseEntity {

	@NotBlank
	@Size(min = 1, max = 20)
	private String name;
	@NotBlank
	@Size(min = 1, max = 20)
	private String email;
	@DateTimeFormat(pattern = "yyyy-mm-dd")
	private Date birthday;
	@ManyToOne
	private User manager;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Level level;
	@Size(min = 1, max = 20)
	private String primarySkill;
	
	private String createByUser;
	private String lastModifiedByUser;
	private Date dateCreate;
	private Date dateLastModified;
}
