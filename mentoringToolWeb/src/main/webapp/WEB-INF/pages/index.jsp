<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Users</h1>
	<c:forEach items="${users}" var="user">
		<dir>
			<div>
				<p>Name: ${user.name}</p>
			</div>
			<div>
				<p>Email:${user.email}</p>
			</div>
			<c:if test="${user.birthday != null}">
				<div>
					<p>
						Bitrhday:
						<fmt:formatDate pattern="yyyy-MM-dd" value="${user.birthday}" />
					</p>
				</div>
			</c:if>
			<c:if test="${user.manager != null}">
				<div>
					<h4>Manager</h4>
					<div>
						<p>Name :${user.manager.name}</p>
					</div>
					<div>
						<p>Email :${user.manager.email}</p>
					</div>
					<div>
						<c:if test="${user.birthday != null}">
							<p>
								Birthday :
								<fmt:formatDate pattern="yyyy-MM-dd" value="${user.birthday}" />
							</p>
						</c:if>
					</div>

					<div>
						<p>Level :${user.manager.level}</p>
					</div>
					<c:if test="${user.manager.primarySkill != null}">
						<div>
							<p>Primary Skill :${user.manager.primarySkill}</p>
						</div>
					</c:if>
					<br>
				</div>
			</c:if>
			<c:if test="${user.level != null}">
				<div>
					<p>Level :${user.level}</p>
				</div>
			</c:if>
			<c:if test="${user.primarySkill != null}">
				<div>
					<p>Primary Skill:${user.primarySkill}</p>
				</div>
			</c:if>
		</dir>
		<form action="delete" method="GET">
			<input type="hidden" value="${user.id}" name="id" />
			<tr>
				<td colspan="2"><input type="submit" value="Delete" /></td>
			</tr>
			</table>
		</form>
		<form action="updatePage" method="GET">
			<input type="hidden" value="${user.id}" name="id" />
			<tr>
				<td colspan="2"><input type="submit" value="Update" /></td>
			</tr>
			</table>
		</form>
		<hr>
	</c:forEach>

	<form action="save" method="POST">
		<table border="0">

			<tr>
				<td><b>Neme</b></td>
				<td><input type="text" name="name" /></td>
			</tr>

			<tr>
				<td><b>Email</b></td>
				<td><input type="text" name="email" /></td>
			</tr>
			<tr>
				<td><b>Birthday</b></td>
				<td><input type="date" name="birthday" /></td>
			</tr>

			<tr>
				<td><b>Primary Skill</b></td>
				<td><input type="text" name="primarySkill" /></td>
			</tr>

			<tr>
				<td>Level</td>
				<td><select name="level">
						<option value="L1">L1</option>
						<option value="L2">L2</option>
						<option value="L3">L3</option>
						<option value="L4">L4</option>
						<option value="L5">L5</option>
				</select></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" value="Save" /></td>
			</tr>
		</table>
	</form>
	<form action="search" method="GET">
		<table border="0">

			<tr>
				<td><b>Search</b></td>
				<td><input type="text" name="param" /></td>
			</tr>

			
			<tr>
				<td colspan="2"><input type="submit" value="Search" /></td>
			</tr>
		</table>
	</form>
	
</body>
</html>