<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<c:if test="${users.isEmpty()}">
		<h1>0 users found!</h1>
	</c:if>
	<c:forEach items="${users}" var="user">
		<dir>
			<div>
				<p>Name: ${user.name}</p>
			</div>
			<div>
				<p>Email:${user.email}</p>
			</div>
			<c:if test="${user.birthday != null}">
				<div>
					<p>
						Bitrhday:
						<fmt:formatDate pattern="yyyy-MM-dd" value="${user.birthday}" />
					</p>
				</div>
			</c:if>
			<c:if test="${user.manager != null}">
				<div>
					<h4>Manager</h4>
					<div>
						<p>Name :${user.manager.name}</p>
					</div>
					<div>
						<p>Email :${user.manager.email}</p>
					</div>
					<div>
						<c:if test="${user.birthday != null}">
							<p>
								Birthday :
								<fmt:formatDate pattern="yyyy-MM-dd" value="${user.birthday}" />
							</p>
						</c:if>
					</div>

					<div>
						<p>Level :${user.manager.level}</p>
					</div>
					<c:if test="${user.manager.primarySkill != null}">
						<div>
							<p>Primary Skill :${user.manager.primarySkill}</p>
						</div>
					</c:if>
					<br>
				</div>
			</c:if>
			<c:if test="${user.level != null}">
				<div>
					<p>Level :${user.level}</p>
				</div>
			</c:if>
			<c:if test="${user.primarySkill != null}">
				<div>
					<p>Primary Skill:${user.primarySkill}</p>
				</div>
			</c:if>
		</dir>
		<hr>
	</c:forEach>
		<a href="/index">main</a>
</body>
</html>