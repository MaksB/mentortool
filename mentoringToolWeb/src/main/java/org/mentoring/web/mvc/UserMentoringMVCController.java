package org.mentoring.web.mvc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.mentoring.model.User;
import org.mentoring.tool.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserMentoringMVCController {

	@Autowired
	private UserService userService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
		dateFormat.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView hello() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		List<User> users = userService.getAllUsers();
		mav.addObject("users", users);
		return mav;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") @Valid User user, ModelMap model) {
		userService.createUser(user);
		return "redirect:index";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteUser(@ModelAttribute("id") Long id, ModelMap model) {
		userService.deleteUser(id);
		return "redirect:index";
	}
	
	@RequestMapping(value = "/updatePage", method = RequestMethod.GET)
	public ModelAndView updateUserPage(@ModelAttribute("id") Long id){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("update");
		User user = userService.getUserById(id);
		mav.addObject("user", user);
		return mav;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("user")@Valid User user){
		userService.updateUser(user);
		return "redirect:index";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView searchUser(@ModelAttribute("param") String param){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("search");
		List<User> users = userService.search(param);
		mav.addObject("users", users);
		return mav;
	}
	

}
