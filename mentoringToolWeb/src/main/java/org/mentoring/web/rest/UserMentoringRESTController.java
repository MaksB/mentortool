package org.mentoring.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.mentoring.model.User;
import org.mentoring.tool.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(consumes=MediaType.APPLICATION_JSON_VALUE)
public class UserMentoringRESTController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public User getUserByid(@PathVariable Long id) throws Exception {
		return userService.getUserById(id);
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public User createUser(@RequestBody @Valid User user) {
		return userService.createUser(user);
	}

	@RequestMapping(value = "/user", method = RequestMethod.PUT)
	public User updateUser(@RequestBody @Valid User user) {
		return userService.updateUser(user);
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable Long id) {
		userService.deleteUser(id);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public List<User> search(@RequestParam(required = false) String param) {
		return userService.search(param);
	}

}
