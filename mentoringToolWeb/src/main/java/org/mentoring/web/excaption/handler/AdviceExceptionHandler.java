package org.mentoring.web.excaption.handler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class AdviceExceptionHandler {

	@ExceptionHandler(value = Exception.class)
	public  ModelAndView toResponse(Exception exception) {
		ModelAndView view = new ModelAndView();
		view.setViewName("404");
		view.addObject("message", exception.getMessage());
		return view;
	}
}
